Dentro del repo se encuentran los archivos de la API con swagger, el .env-example (quitar "-example") y el dataset de las películas.

Si la consola esta ubicada dentro del proyecto, el comando para importar el dataset es:
    mongoimport --db test-typescript --collection batman_movies --file batman_films.json --jsonArray

La URL de la documentación en swagger es:
    https://app.swaggerhub.com/apis/Julian-Software/test-typescript/1.0.0

Cualquier duda que tengan respecto a la prueba por favor hagánmela saber mediante mi cuenta de
getonbrd.com o a mi correo julian_ag96@hotmail.com
