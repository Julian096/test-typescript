{
  "openapi": "3.0.0",
  "info": {
    "title": "Películas de Batman API",
    "description": "Una API de películas de Batman",
    "contact": {
      "name": "Julián",
      "email": "julian_ag96@hotmail.com"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    },
    "version": "1.0.0"
  },
  "servers": [
    {
      "url": "https://virtserver.swaggerhub.com/Julian-Software/test-typescript/1.0.0",
      "description": "SwaggerHub API Auto Mocking"
    }
  ],
  "tags": [
    {
      "name": "admins",
      "description": "Secured Admin-only calls"
    },
    {
      "name": "developers",
      "description": "Operations available to regular developers"
    }
  ],
  "paths": {
    "/getMovie/{id}": {
      "get": {
        "tags": [
          "movie"
        ],
        "summary": "Regresa una película",
        "description": "Endpoint que recibe el id por parámetro y regresa la película asociada a dicho id.\n",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "id de la película",
            "required": true,
            "style": "simple",
            "explode": false,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Petición exitosa",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/BatmanMovie"
                }
              }
            }
          },
          "400": {
            "description": "Parámetro inválido",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string",
                  "example": "Parámetro inválido"
                }
              }
            }
          },
          "404": {
            "description": "Película no encontrada",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string",
                  "example": "Película no encontrada"
                }
              }
            }
          }
        }
      }
    },
    "/getAllMovies": {
      "get": {
        "tags": [
          "movie"
        ],
        "summary": "Regresa todas las películas",
        "description": "Endpoint que regresa todas las películas guardadas.\n",
        "responses": {
          "200": {
            "description": "Petición exitosa",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ArrayOfBatmanMovie"
                }
              }
            }
          },
          "404": {
            "description": "Película no encontrada",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string",
                  "example": "Películaa no encontrada"
                }
              }
            }
          }
        }
      }
    },
    "/newMovie": {
      "post": {
        "tags": [
          "movie"
        ],
        "summary": "Guarda una nueva película",
        "description": "Endpoint que permite crear y guardar una nueva película.\n",
        "requestBody": {
          "description": "Objeto de tipo Película",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/BatmanMovie"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Regresa el objeto película si la petición fue exitosa",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/BatmanMovie"
                }
              }
            }
          },
          "500": {
            "description": "Error en servidor",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string",
                  "example": "Error al guardar información"
                }
              }
            }
          }
        }
      }
    },
    "/deleteMovie/{id}": {
      "delete": {
        "tags": [
          "movie"
        ],
        "summary": "Regresa una película",
        "description": "Endpoint que recibe el id por parámetro y elimina la película asociada a dicho id.\n",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "id de la película",
            "required": true,
            "style": "simple",
            "explode": false,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Petición exitosa",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string",
                  "example": "Película borrada"
                }
              }
            }
          },
          "400": {
            "description": "Parámetro inválido",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string",
                  "example": "Parámetro inválido"
                }
              }
            }
          },
          "404": {
            "description": "Película no encontrada",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string",
                  "example": "Película no encontrada"
                }
              }
            }
          }
        }
      }
    },
    "/updateMovie/{id}": {
      "put": {
        "tags": [
          "movie"
        ],
        "summary": "Regresa una película",
        "description": "Endpoint que recibe el id por parámetro y la opinion dentro del body para actualizar la película\n",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "id de la película",
            "required": true,
            "style": "simple",
            "explode": false,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "description": "Objeto con la opinion",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/body"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Petición exitosa",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string",
                  "example": "Película actualizada"
                }
              }
            }
          },
          "400": {
            "description": "Parámetro inválido",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string",
                  "example": "Parámetro inválido"
                }
              }
            }
          },
          "404": {
            "description": "Película no encontrada",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string",
                  "example": "Película no encontrada"
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "BatmanMovie": {
        "type": "object",
        "properties": {
          "_id": {
            "type": "string",
            "example": "6080797dc5377fd6b982b396"
          },
          "Year": {
            "type": "integer",
            "example": 2017
          },
          "Title": {
            "type": "string",
            "example": "Justice League"
          },
          "Rated": {
            "type": "string",
            "example": "PG-13"
          },
          "Director": {
            "type": "string",
            "example": "Zack Snyder"
          },
          "Released": {
            "type": "string",
            "example": "11/17/2017"
          },
          "Writer": {
            "type": "string",
            "example": "Jerry Siegel (Superman created by), Joe Shuster (Superman created by), Chris Terrio (story by), Zack Snyder (story by), Chris Terrio (screenplay by), Joss Whedon (screenplay by), Gardner Fox (Justice League of America created by), Bob Kane (Batman created by), Bill Finger (Batman created by), William Moulton Marston (Wonder Woman created by), Jack Kirby (Fourth World created by)"
          },
          "Production": {
            "type": "string",
            "example": "Warner Bros. Pictures"
          },
          "Actors": {
            "type": "string",
            "example": "Ben Affleck, Henry Cavill, Amy Adams, Gal Gadot"
          },
          "Runtime": {
            "type": "string",
            "example": "120 min"
          },
          "Awards": {
            "type": "string",
            "example": "2 wins & 13 nominations."
          },
          "Tmdb_Votes": {
            "type": "string",
            "example": "352,866"
          },
          "Tmdb-Rating": {
            "type": "number",
            "format": "float",
            "example": 6.4
          },
          "RottenTomatoScore": {
            "type": "string",
            "example": "40%"
          },
          "My_opinion": {
            "type": "string",
            "example": "Good"
          }
        }
      },
      "ArrayOfBatmanMovie": {
        "type": "array",
        "items": {
          "$ref": "#/components/schemas/BatmanMovie"
        }
      },
      "body": {
        "type": "object",
        "properties": {
          "My_opinion": {
            "type": "string",
            "example": "Good"
          }
        }
      }
    }
  }
}