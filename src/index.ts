import * as dotenv from 'dotenv';
dotenv.config();
import express, { Application } from 'express';
import cors from 'cors';
import helmet from 'helmet';
import { batmanRoutes } from "./routes";

if (!process.env.PORT) {
    process.exit(1);
}

class App {
    private app: Application;
    private port: number;

    constructor() {
        this.port = parseInt(process.env.PORT!, 10);
        this.app = express();
        this.config();
        this.routes();
    }

    private config(): void {
        this.app.use(helmet());
        this.app.use(cors());
        this.app.use(express.json());
    }

    private routes(): void {
        this.app.use(batmanRoutes.getRouter());
    }

    public runServer(): void {
        this.app.listen(this.port, () => console.log(`Listening on port ${this.port}`))
    }
}

const app = new App();
app.runServer();