export const BatmanMovieSchema = {
    validator: {
        $jsonSchema: {
            bsonType: "object",
            additionalProperties: false,
            required: [
                "Year",
                "Title",
                "Rated",
                "Director",
                "Released",
                "Writer",
                "Production",
                "Actors",
                "Runtime",
                "Awards",
                "Imdb_Votes",
                "Imdb_Rating",
                "RottenTomatoScore",
                "My_opinion"
            ],
            properties: {
                _id: {},
                Year: {
                    bsonType: "int",
                    description: "must be a int and is required"
                },
                Title: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                Rated: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                Director: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                Released: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                Writer: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                Production: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                Actors: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                Runtime: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                Awards: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                Imdb_Votes: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                Imdb_Rating: {
                    bsonType: "double",
                    description: "must be a double and is required"
                },
                RottenTomatoScore: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                My_opinion: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },                
            }
        }
    }
};