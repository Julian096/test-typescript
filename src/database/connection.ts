import { Db, MongoClient } from 'mongodb';

class ConnectionDB {
    private uri: string;
    private database: string;
    private client: MongoClient;

    constructor() {
        this.uri = process.env.MONGO_URI!;
        this.database = process.env.DATABASE!;
        this.client = this.createClient();
        this.run()
    }

    private createClient(): MongoClient {
        return new MongoClient(this.uri, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
    }

    public getDatabase(): Db {
        return this.client.db(this.database);
    }

    public async run() {
        try {
            await this.client.connect();
            console.log("Database connected");
        } catch(error){ console.log(error) }
    }

}

export const connectionDB = new ConnectionDB();