import { Router } from "express";
import { getAllMovies, getMovie, newMovie, deleteMovie, updateMovie } from "../controllers/batmanMovies.controller";

class BatmanRoutes {
    private router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    public getRouter(): Router {
        return this.router;
    }

    private routes(): void {
        this.router.get("/getMovie/:id", getMovie);
        this.router.get("/getAllMovies", getAllMovies);
        this.router.post("/newMovie", newMovie);
        this.router.delete("/deleteMovie/:id", deleteMovie);
        this.router.put("/updateMovie/:id", updateMovie);
    }
}

export const batmanRoutes = new BatmanRoutes();