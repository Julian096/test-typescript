import { Collection } from "mongodb";
import { connectionDB } from "../database/connection";
import { BatmanMovieSchema } from "../schemas/batmanMovies.schema";

const db = connectionDB.getDatabase();

let batmanMovies: Collection;

db.createCollection("batman_movies", BatmanMovieSchema)
.then((collection: Collection) => {
    batmanMovies = collection;
})
.catch(error => console.log(error));

export { batmanMovies };