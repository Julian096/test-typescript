export interface BatmanMovieInterface {
    Year: number;
    Title: string;
    Rated: string;
    Director: string;
    Release: string;
    Writer: string;
    Production: string;
    Actors: string;
    Runtime: string;
    Awards: string;
    Imdb_Votes: string;
    Imdb_Rating: number;
    RottenTomatoScore: string;
    My_opinion: string;
}