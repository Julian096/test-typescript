import { ObjectId } from "bson";
import { Request, Response } from "express";
import { batmanMovies } from "../models/batmanMovies.model";
import { BatmanMovieInterface } from "../interfaces/batmanMovie.interface";

export const getAllMovies = async (req: Request, res: Response): Promise<Response | undefined> => {
    try {
        const movies = await batmanMovies.find().toArray();
        return movies.length !== 0 ? res.json(movies) : res.status(404).json("No hay películas");
    } catch (error) { console.log(error) }
}

export const getMovie = async (req: Request, res: Response): Promise<Response | undefined> => {
    try {
        const { id } = req.params;
        if (id.match(/^[0-9a-fA-F]{24}$/)) {
            const movie = await batmanMovies.findOne(new ObjectId(id));
            return movie !== null ? res.json(movie) : res.status(404).json("Película no encontrada");
        } else {
            res.status(400).json("Parámetro inválido")            // nope    ;
        }
    } catch (error) { console.log(error) }
}

export const newMovie = async (req: Request, res: Response): Promise<Response | undefined> => {
    try {
        const newMovie: BatmanMovieInterface = req.body;
        const movie = await batmanMovies.insertOne(newMovie);
        return res.json(movie.ops[0]);
    } catch (error) {
        console.log(error);
        res.status(500);
        res.json("Error al guardar información");
    }
}

export const deleteMovie = async (req: Request, res: Response): Promise<Response | undefined> => {
    try {
        const { id } = req.params;
        if (id.match(/^[0-9a-fA-F]{24}$/)) {
            const movie = await batmanMovies.deleteOne({ _id: new ObjectId(id) });
            return movie.deletedCount !== 0 ? res.json("Pelicula borrada") : res.status(404).json("Pelicula no encontrada");
        } else {
            res.status(400).json("Parámetro inválido");
        }
    } catch (error) { console.log(error) }
};

export const updateMovie = async (req: Request, res: Response): Promise<Response | undefined> => {
    const { id } = req.params;
    if (id.match(/^[0-9a-fA-F]{24}$/)) {
        const { my_opinion } = req.body;
        const update = {
            My_opinion: my_opinion
        }
        const movie = await batmanMovies.findOneAndUpdate({ _id: new ObjectId(id) }, { $set: update });
        return movie.value !== null ? res.json("Pelicula actualizada") : res.status(404).json("Pelicula no encontrada");
    } else {
        res.status(400).json("Parámetro inválido");
    }
}